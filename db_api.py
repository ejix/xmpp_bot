import sqlite3


class DataBase():

    def __init__(self, jid: str) -> None:
        self.jid = jid
        self.conn = sqlite3.connect('users_data.sqlite3')
        self.cur = self.conn.cursor()
        self.cur.execute(f''' CREATE TABLE IF NOT EXISTS '{jid}'
                         (id INTEGER PRIMARY KEY,
                         link TEXT)''')

    def add_link(self, link: str) -> None:
        self.cur.execute(f''' INSERT INTO '{self.jid}' (link)
                         VALUES ('{link}')''')
        self.conn.commit()
        self.conn.close()

    def get_links(self) -> list:
        links = self.cur.execute(f'''SELECT link FROM '{self.jid}' ''')
        links = links.fetchall()
        out = []
        for link in links:
            out.append(link[0])
        return out

    def remove_link(self, link: str) -> None:
        self.cur.execute(f''' DELETE FROM '{self.jid}'
                         WHERE name = '{link}' ''')
        self.conn.commit()
        self.conn.close()

    def remove_table(self) -> None:
        self.cur.execute(f''' DROP TABLE IF EXISTS '{self.jid}' ''')
        self.conn.commit()
        self.conn.close()
